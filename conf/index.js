'use strict'

const PRODUCTION = process.env.NODE_ENV === 'production'

module.exports = {
  port: 3001
, host: '0.0.0.0'
, pino: {
    prettyPrint: !PRODUCTION
  , extreme: PRODUCTION
  , level: 'trace'
  }
, nats: {
    servers: 'nats://0.0.0.0:4222'
  , user: null
  , pass: null
  }
, redis: {
    host: 'redis://0.0.0.0:6379'
  }
}
