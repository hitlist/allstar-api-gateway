'use strict'

const GraphQLJSON = require('graphql-type-json')
const Tools = require('graphql-tools')
const CustomTypes = require('graphql-custom-types')
const mocks = require('../mocks')
const hemera = require('../../lib/hemera')

const {
  makeExecutableSchema
, addMockFunctionsToSchema
, mergeSchemas
} = Tools

const {
  GraphQLEmail
, GraphQLUUID
, GraphQLDateTime
} = CustomTypes

const TYPES = `
scalar Email
scalar UUID
scalar DateTime
scalar JSON

type Name {
  first: String
  last: String
}

type User {
  username: String
  name: Name
  created: DateTime
  email: Email
  permissions: JSON
  superuser: Boolean
  active: Boolean
  roles: [String]
  pk: UUID
  allstar_auth_user_id: ID!
}

type Query {
  user(pk: Email): User
  users(first_name: String): [User]
}
`;
const resolveFunctions = {
  JSON: GraphQLJSON
, Email: GraphQLEmail
, DateTime: GraphQLDateTime
, UUID: GraphQLUUID
, Query: {
    users(_, args) {
      return hemera.act({
        topic: 'user'
      , cmd: 'list'
      , version: 'v1'
      , meta$: {
          user: {
            superuser: true
          }
        }
      })
    }
  , user(_, args) {
      if (!args.pk) {
        const err = new Error('Must specify a user id (pk) to look up')
        err.name = err.code = 'EBADREQUEST'
        throw err
      }
      return hemera.act({
        topic: 'user'
      , cmd: 'get'
      , version: 'v1'
      , user_id: args.pk
      , meta$: {
          user: {
            superuser: true
          }
        }
      })
    }
  }
};

const UserSchema = makeExecutableSchema({typeDefs: TYPES, resolvers: resolveFunctions})

/*
addMockFunctionsToSchema({
  schema: UserSchema
, mocks: mocks.user
})
*/
module.exports = mergeSchemas({ schemas: [UserSchema] })
