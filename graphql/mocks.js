'use strict';

const casual = require('casual')

const User = {
  String: () => {
    return casual.username
  }
, Name: () => {
    return {
      first: casual.first_name
    , last: casual.last_name
    }
  }
, Email: () => {
    return casual.email
  }
, JSON: () => {
    return {
      auth: {
        user: {
          create: casual.boolean
        , delete: casual.boolean
        , update: casual.boolean
        }
      }
    , league: {
        league: {
          create: casual.boolean
        , delete: casual.boolean
        , update: casual.boolean
        }
      , team: {
          create: casual.boolean
        , delete: casual.boolean
        , update: casual.boolean
        }
      }
    }
  }
, Boolean: () => {
    return casual.boolean
  }
, DateTime: () => {
    return casual.date(format='YYYY-MM-DDTHH:mm:ssZ')
  }
, UUID: () => {
    return casual.uuid
  }
}
module.exports = {
  user: User
}
