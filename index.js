'use strict'

const conf = require('keef')
const Hapi = require('hapi')
const Boom = require('boom')
const parse = require('@allstar/parse-hosts')
const {graphqlHapi, graphiqlHapi} = require('apollo-server-hapi')
const auth = require('./lib/auth-proxy')
const log = require('./lib/log')
const SCHEMA = require('./graphql/schema')

const server = new Hapi.Server()
const nats = conf.get('nats')

server.connection({
  port: conf.get('port')
, host: conf.get('host')
})

server.auth.scheme('proxied', auth)
server.auth.strategy('default', 'proxied')

const plugins = [{
  register: graphqlHapi
, options: {
    path: '/'
  , graphqlOptions: {
      schema: SCHEMA
    , pretty: true
    , context: {}
    }
  , route: {
      cors: true
    }
  }
}]


if (process.env.NODE_ENV !== 'production') {
  plugins.push({
    register: graphiqlHapi
  , options: {
      path: '/debug'
    , graphiqlOptions: {
        endpointURL: '/'
      }
    }
  })
}

server.register(plugins, (err) => {
  if (err) {
    log.error(err.message)
    throw err
  }
  server.on('request-error', (request, err) => {
    log.error(err.message)
    log.trace(err.stack)
  })
  server.on('tail', (request) => {
    log.debug(`${request.method} ${request.path} ${(request.info.responded - request.info.received)} ms`)
  })
  server.start((err) => {
    if (err) {
      log.error(err.message)
      throw err
    }
    log.info(`server running at: ${server.info.uri}`);
  });
})

process.once('SIGTERM', onSignal)
process.once('SIGINT', onSignal)

function onSignal() {
  log.info('shutting down http server')
  server.stop({timeout: 2000}, (err) => {
    if (err) throw err
    log.info('server shutdown')
    process.exit()
  })
}
