const redis = require('redis')
const conf = require('keef')

const MAX_RETRIES = 100

const client = redis.createClient(conf.get('redis:host'), {
  retry_strategy: retry
})

client.on('reconnecting', () => {
  console.error('redis reconnecting')
})

module.exports = client

function retry(opts) {
  if (opts.err) {
    console.warn('unable to connect to redis', opts.error)
  }

  const {attempt, times_connected, total_retry_time} = opts
  if (opts.attempts > MAX_RETRIES) {
    return undefined
  }
  return 100
}
