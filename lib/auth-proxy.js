'use strict'

const Boom = require('boom')
const storage = require('./storage')
module.exports = function(server, options) {
  return {
    api: {
      settings: {
        ttl: 1000 * 60 * 5
      }
    }
  , authenticate: async function(request, reply) {
      const req = request.raw.headers.authorization
      if (!authorization) {
        return reply(Boom.unauthorizaed())
      }
      const user = await store.get(authorization)
      if (!user) return reply(Boom.unauthorizaed())
      reply.continue({credentials: { user }})
    }
  }
}
