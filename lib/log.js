'use strict'
const conf = require('keef')

module.exports = require('pino')({
  level: 'trace'
, enabled: true
, prettyPrint: conf.get('pino:prettyPrint')
, name: 'gateway'
, extreme: conf.get('pino:extreme')
})
