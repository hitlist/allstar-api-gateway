/*jslint esnext: true, laxcomma: true, node: true, asi: true, unused: true */
const conf = require('keef')
const Session = require('../session')

module.exports = class SessionStore {
  constructor(db) {
    this.db = db
    this.locks = new Set
  }

  connect(cb) {
      this.db.once('ready', (err) => {
        cb(err)
      })
  }

  disconnect() {
    const multi = this.db.multi()

    for (const lock of this.locks) {
      multi.del(lock)
    }

    this.locks.clear()
    multi.exec(() => {
      this.db.quit()
    })
  }

  get(token, cb) {
    const key = dbKey(token)
    this.db.get(key, (err, str) => {
      if (err) return cb(err)
      if (!str) {
        const error = new Error('Key not found')
        error.code = 'ENOENT'
        return cb(error)
      }

      const out = jsonParse(str)
      Session.create(out, cb)
    })
  }

  getAndLock(token, count, cb){
    const key = `lock:${dbKey(token)}`
    this._getAndLock(key, 0, (err) => {
      if (err) return cb(err)
      cb()
    })
  }

  unlock(token, cb){
    const key = `lock:${dbKey(token)}`
    this.locks.delete(key)
    this.db.del(key, cb)
  }

  update(token, fn, cb){
    this.getAndLock(token, (err) => {
      if (err) return cb(err)

      this.get(token, (err, sess) => {
        if (err) {
          return this.unlock(token, () => {
            cb(err)
          })
        }

        fn(sess, (err, session, can_consume) => {
          if (err) {
            return this.unlock(token, () => {
              cb(err)
            })
          }

          const ttl = conf.get('session:ttl') || 0
          this.set({session, token, ttl}, (err) => {
            if (!can_consume) {
              const er = new Error('RateLimite Exceeded')
              er.code = 'ERATELIMIT'
              return cb(err, session)
            }
            cb(null, session)
          })
        })
      })
    })
  }

  touch(token, cb){
    this.update(token, (session, cb) => {
      const ttl = conf.get('session:ttl') || 0
      if (ttl !== 0) {
        const expires = Date.now() + (ttl * 1000)
        session.expires = expires
        session.consume()
        cb(null, session, can_consume)
      }
    }, (err, session) => {
      // pino...
      if (err) console.error(err)
      this.unlock(token, () => {
        cb(err, session)
      })
    })
  }

  _getAndLock(key, count, cb) {
    if (count > 10) {
      const error = new Error('unable to acquire session lock')
      error.code = 'ENOLOCK'
      return setImmediate(cb, error)
    }
    this.db.setnx(key, (err) => {
      if (err) return this._getAndLock(key, count + 1, cb)
      this.locks.add(key)
      cb()
    })
  }
  clearStale(opts, cb) {
    setImmediate(cb)
  }
  set(opts, cb){
    const { session, token, ttl } = opts
    const key = dbKey(token)
    const val = JSON.stringify(session.toDB())
    const done = (err, res, val) => {
      if (err) return cb(err)
      if (res !== 1) {
        const error = new Error('Key already exists')
        error.code = 'EEXIST'
        return done(error)
      }
      this.clearStale({ session, token }, cb)
    }

    if (ttl == 0) {
      this.db.setnx(key, val, done)
    } else {
      this.db.setex(key, ttl, val, done)
    }
  }
}

function dbKey(token) {
  return `allstar:session:${token}`
}

function jsonParse (str) {
  try {
    return JSON.parse(str)
  } catch (_) {
    return undefined
  }
}
