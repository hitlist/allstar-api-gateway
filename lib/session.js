/*jslint esnext: true, laxcomma: true, node: true, asi: true */
'use strict'

module.exports = class Session {
  constructor(opts) {
    const {
      roles
    , permissions
    , id
    , email
    , username
    , expires
    , rate_limit
    } = opts

    this.roles = roles
    this.permissions = permissions
    this.id = id
    this.email = email
    this.username = username
    this.expires = expires
    this.rate_limit = rate_limit
  }

  static create(obj, cb) {
    if (obj.meta_data === null || typeof obj.meta_data !== 'object') {
      const error = new Error('Invalid session')
      error.code = 'ENOSESSION'
      error.status = 401
      return cb(error)
    }

    const meta = obj.meta_data
    setImmediate(cb, null, new Session({
      roles: meta.roles
    , permissions: meta.permissions
    , expires: obj.expires
    , id: meta.id
    , email: meta.email
    , rate_limit: obj.rate_limie
    }))
  }

  consume() {
    const rl = this.ratelimit
    if (rl === null) return true

    const now = Date.now()

    if (now < rl.time) {
      rl.time = now - 1000
    }

    if (rl.tokens < rl.capacity) {
      const delta = rl.fill_rate * ((now - rl.time) / 1000)
      rl.tokens = Math.min(rl.capacity, rl.tokens + delta)
    }

    rl.time = now

    if (rl.tokens > 1) {
      rl.tokens -= 1
      return true
    }

    return false
  }

  toJSON() {
    return {
      meta_data: {
        roles: this.roles
      , permissions: this.permissions
      , id: this.id
      , email: this.email
      , username: this.username
      }
    , expires: this.expires
    , rate_limit: this.rate_limit
    }
  }
  toMetaData() {
    return {
      email: this.email
    , roles: this.roles
    , id: this.id
    , permissions: this.permissions
    , username: this.username
    }
  }

  toDB() {
    return {
      meta_data: {
        roles: this.roles
      , permissions: this.permissions
      , id: this.id
      , email: this.email
      , username: this.username
      }
    , expires: this.expires
    , rate_limit: this.rate_limit
    , type: 'session'
    }
  }
}
