'use strict'

const uuid = require('uuid')
const {test} = require('tap')
const RedisStore = require('../../../lib/storage/redis')
const redis = require('../../../lib/db/redis')
const Session = require('../../../lib/session')
test('redis', (t) => {
  const store = new RedisStore(redis)
  t.test('#connect()', (tt) => {
    store.connect((err) => {
      tt.error(err)
      tt.end()
    })
  })
  t.test('#get()', (tt) => {
    tt.test('invalid format', (ttt) => {
      const key = 'allstar:session:4444'
      redis.set(key, JSON.stringify({
        a: 1
      }), (err) => {
        ttt.error(err)
        store.get('4444', (err) => {
          ttt.type(err, Error)
          ttt.equal(err.code, 'ENOSESSION')
          ttt.end()
        })
      })
    })
    tt.test('not found', (ttt) => {
      store.get('abacadaba', (err) => {
        ttt.type(err, Error)
        ttt.equal(err.code, 'ENOENT')
        ttt.end()
      })
    })
    tt.end()
  })
  t.test('set()', (tt) => {
    const token = 'sdfafdsfadsfsd'
    const ttl = 0
    const session = new Session({
      permissions: {}
    , uuid: uuid.v4()
    , roles: ['admin']
    , expires: Date.now() + 200000
    , ratelimit: null
    })

    tt.test('success', (ttt) => {
      store.set({ session, token, ttl }, (err) => {
        ttt.error(err)
        ttt.end()
      })
    })

    tt.test('key exists', (ttt) => {
      store.set({ session, token, ttl }, (err) => {
        ttt.type(err, Error)
        ttt.equal(err.code, 'EEXIST', 'err.code')
        ttt.end()
      })
    })

    tt.end()
  })


  t.test('disconnect', (tt) => {
    redis.flushdb(redis.print)
    store.disconnect()
    tt.end()
  })
  t.end()
})
